import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import tanling.matplot_4j.ColorHelper;
import tanling.matplot_4j.d3d.base.pub.ColorStyle;
import tanling.matplot_4j.d3d.base.pub.Range;
import tanling.matplot_4j.d3d.base.pub.TopBottomColorStyle;
import tanling.matplot_4j.d3d.facade.Function;
import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;

public class FunctionPointMoveDemo {

	static Thread thread;

	public static void main(String[] args) {

		final MatPlot3DMgr mgr = new MatPlot3DMgr(true);

		mgr.setDataInputType(MatPlot3DMgr.DATA_TYPE_FUNCTION3D);

		mgr.setShowType(MatPlot3DMgr.SHOW_TYPE_DOTS);

		mgr.setScaleZ(2.5);
		mgr.setScaleX(1.2);
		mgr.setScaleY(1.2);

		mgr.getProcessor().setCoordinateSysShowType(mgr.getProcessor().COORDINATE_SYS_ALWAYS_FURTHER);
		mgr.getProcessor().setCoordinateSysTransformationType(mgr.getProcessor().COORDINATE_RANGE_AUTO_FIT);
		
		JFrame jf = new JFrame("InternalFrameTest");
		try {
			String lookAndFeel = UIManager.getSystemLookAndFeelClassName();
			UIManager.setLookAndFeel(lookAndFeel);
			SwingUtilities.updateComponentTreeUI(jf);
		} catch (Exception e) {
			e.printStackTrace();
		}

		//可以将Panel对象加入到自己的GUI框架中
		jf.setContentPane(mgr.getPanel());

		jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
		jf.setSize(800, 800);
		jf.setVisible(true);

		class Int {
			int i = 0;
		}

		final Int ii = new Int();

		final ColorStyle cs=new TopBottomColorStyle(ColorHelper.COLD,Color.GREEN,Color.YELLOW,ColorHelper.WARM);
		
		thread = new Thread(new Runnable() {

//			@Override
			public void run() {
				int count = 0;

				while (true) {

					ii.i++;

					Function f1 = new Function() {
						public double f(double x, double y) {
							return 0.5 * (Math.sin((double) ii.i / 100) + 1.01) * Math.sin(y + ((double) ii.i / 15))
									* Math.cos(x);
						}
					};

					mgr.getProcessor().clear();

					mgr.addData(f1, new Range(-1, 5), new Range(-1, 5), 35, 35,cs);

					if (count == 0) {
						mgr.fitScreem();
					}

					try {
						mgr.updateView(15);
					} catch (Exception e) {
						e.printStackTrace();
					}

					count++;

					Thread.yield();
				}
			}

		});

		thread.start();

	}
}
