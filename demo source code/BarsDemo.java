import java.util.Random;
import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;

public class BarsDemo {

	public static void main(String[] args) {

		MatPlot3DMgr mgr = new MatPlot3DMgr(true);

		mgr.setDataInputType(MatPlot3DMgr.DATA_TYPE_MATRIX);

		Random ra=new Random();
		double[][] ds1 = new double[][] { { 3, 4, 5, 6, 7 }, { 2, 3, 4, 9, 6 }, { 1, 2, 3, 8, 5 } };

		double[][] ds2 = new double[][] {
				{ 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(), 6 + ra.nextDouble(),
						7 + ra.nextDouble() },
				{ 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(),
						6 + ra.nextDouble() },
				{ 1 + ra.nextDouble(), 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(),
						5 + ra.nextDouble() } };

		double[][] ds3 = new double[][] {
				{ 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(), 6 + ra.nextDouble(),
						7 + ra.nextDouble() },
				{ 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(),
						6 + ra.nextDouble() },
				{ 1 + ra.nextDouble(), 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(),
						5 + ra.nextDouble() } };

		double[][] ds4 = new double[][] {
				{ 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(), 6 + ra.nextDouble(),
						7 + ra.nextDouble() },
				{ 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(),
						6 + ra.nextDouble() },
				{ 1 + ra.nextDouble(), 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(),
						5 + ra.nextDouble() } };

		double[][] ds5 = new double[][] {
				{ 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(), 6 + ra.nextDouble(),
						7 + ra.nextDouble() },
				{ 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(),
						6 + ra.nextDouble() },
				{ 1 + ra.nextDouble(), 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(),
						5 + ra.nextDouble() } };

		mgr.addData("项目1", ds1);
		mgr.addData("项目2", ds2);
		mgr.addData("项目3", ds3);
		mgr.addData("项目4", ds4);
		mgr.addData("项目5", ds5);
		
		mgr.setTitle("Demo : 多层柱状图");

		mgr.setScaleZ(8);
		mgr.setScaleX(1.2);
		mgr.setScaleY(2);
		
		mgr.setSeeta(0.3);
		mgr.setBeita(1.2);
		
		mgr.getProcessor().setRulerLabelsX(new String[] { "A区", "B区", "C区", "D区", "E区" });
		mgr.getProcessor().setRulerLabelsY(new String[] { "2018", "2017", "2016" });
		
		mgr.getProcessor().setXName("X_区域");
		mgr.getProcessor().setYName("Y_年份");
		mgr.getProcessor().setZName("Z_指标");
		
		mgr.getProcessor().setCoordinateSysShowType(mgr.getProcessor().COORDINATE_SYS_STABLE);

		mgr.show();
	}

}
